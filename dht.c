#include <gpiod.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

const int HIGH = 1;
const int LOW = 0;
// const char* "dht"= "dht";

struct data
{
    int temp;
    int humidity;
};

struct gpioInfo
{
    struct gpiod_line * line;
    struct gpiod_chip * chip;
};

static void * read_sensor(void * arg){

    printf("oui\n");
    long tmp = 0;

    struct gpioInfo * info = (struct gpioInfo *)arg;
    struct gpiod_line_event event;
    int i=0;
    
    if(gpiod_line_request_output(info->line,"dht",HIGH)<0)
    {
        perror("request output failed\n");
        // gpiod_line_release(info->line);
        // gpiod_chip_close(info->chip);
        // return 0;
    }

    sleep(1);
    gpiod_line_set_value(info->line,0);
    // clock_gettime(CLOCK_REALTIME, &startTime);
    usleep(18000);
    gpiod_line_set_value(info->line,1);
    // clock_gettime(CLOCK_REALTIME, &stopTime);
    // printf("time in ns \t: %ld, usleep 18k\n",(stopTime.tv_nsec-startTime.tv_nsec)/1000);
    gpiod_line_release(info->line);   
    // gpiod_line_request_input(line,argv[0]);
    if(gpiod_line_request_both_edges_events(info->line,"dht")<0)
    {
        perror("request event failed\n");
        // gpiod_line_release(info->line);
        // gpiod_chip_close(info->chip);
        // return 0;
    }

    while(i<=39)
    {
        gpiod_line_event_wait(info->line,NULL);
        gpiod_line_event_read(info->line,&event);
        if (tmp == 0)
        {
            printf("time : %ld, i : %d\n",(0,i));
        }
        else
        {
            printf("time : %ld, i : %d\n",((event.ts.tv_nsec-tmp)/1000),i);
        }
        tmp = event.ts.tv_nsec;
        i++;
        if (i == 39)
        {
            break;
        }
    } 
    // struct timespec startTime;
    // struct timespec stopTime;
    //     while(i<40)
    // {
    //     gpiod_line_event_wait(info->line,NULL);

    //     gpiod_line_event_read(info->line,&event);

    //     if (event.event_type == 1)
    //     {
    //         clock_gettime(CLOCK_REALTIME, &startTime);
    //     }
    //     else
    //     {
    //         clock_gettime(CLOCK_REALTIME, &stopTime);
    //         printf("HIGH time : %ld, bit : %d\n",(stopTime.tv_nsec-startTime.tv_nsec)/1000,i);

    //         i++;
    //     }
        
    // } 
}

int main(int argc,char * argv[])
{
    // printf("%d\n",SCHED_FIFO);

    
    // char chipname = "gpiochip0";
    unsigned int pin = 17;

    struct gpioInfo info;
    
    pthread_attr_t attr;
    struct sched_param param;

    pthread_t sensorThread;
    int error;


    error = pthread_attr_init (&attr);
    if(error)
    {
        perror("init attr failed\n");
        return 0;
    }

    error = pthread_attr_setschedpolicy(&attr,1);
    if(error)
    {
        perror("set policy failed\n");
        return 0;
    }

    error = pthread_attr_getschedparam(&attr,&param);
    if(error)
    {
        perror("getschedparam failed\n");
        return 0;
    }

    (param.sched_priority) = 99;

    error = pthread_attr_setschedparam (&attr,&param);
    if(error == ENOTSUP)
    {
        perror("setschedparam failed ENOTSUP\n");
        return 0;
    }else if(error == EINVAL)
    {
        perror("setschedparam failed EINVAL\n");
        return 0;
    }

    info.chip = gpiod_chip_open_by_number(0);
    if(!info.chip)
    {
        perror("Open chip failed\n");
        gpiod_chip_close(info.chip);
        return 0;
    }

    info.line = gpiod_chip_get_line(info.chip,pin);
    if(!info.line)
    {
        perror("Get line failed\n");
        gpiod_line_release(info.line);
        gpiod_chip_close(info.chip);
        return 0;
    }
    // printf("oui\n");
    // error = pthread_create(&sensorThread, &attr, &read_sensor,&info);
    error = pthread_create(&sensorThread, &attr, &read_sensor,&info);
    if (error != 0) {
        printf("Error to create thread\n");
    }

    while(1){}
    gpiod_line_release(info.line);
    gpiod_chip_close(info.chip);
    return 0;
}